# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term "kitty"
set $fileman "thunar"
# set $browser "brave-browser --enable-features=UseOzonePlatform --ozone-platform=wayland"
# set $browser "vivaldi --enable-features=UseOzonePlatform --ozone-platform=wayland"
#set $browser "MOZ_ENABLE_WAYLAND=1 firefox"
set $browser "flatpak run one.ablaze.floorp"
#set $bemenu "bemenu-run --hb '#467b96' --hf '#dfdfdf' --tb '#467b96' --tf '#dfdfdf' -H 20 --fn 'Hack' -p 'Run:'"
#set $bemenu-for-drun "bemenu --hb '#467b96' --hf '#dfdfdf' --tb '#467b96' --tf '#dfdfdf' -H 20 --fn 'Hack' -p 'Desktop:'"
set $bemenu "~/.local/bin/my-bemenu -r"
set $bemenu-for-drun "~/.local/bin/my-bemenu -d"
set $drun "~/.local/bin/my-bemenu -d"
#set $drun j4-dmenu-desktop --dmenu=$bemenu-for-drun --term=kitty --usage-log=$HOME/.local/share/j4-dmenu-desktop.log --no-generic
#set $browser "MOZ_ENABLE_WAYLAND firefox"
set $logout "~/.local/bin/logout-script"

# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
#set $menu dmenu_path | wmenu | xargs swaymsg exec --

#
# SWAYFX CONFIGURATION!!!!
#
corner_radius 5



### Output configuration
#
# Default wallpaper (more resolutions are available in @datadir@/backgrounds/sway/)
output * bg /home/shatterstone/wallpapers/63.jpg fill
#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

input type:keyboard { 
    xkb_layout gb
}

input type:touchpad {
    tap enabled
    tap_button_map lmr
}

default_border pixel 2

gaps inner 10
### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+Shift+c kill

    # Start your launcher
    #bindsym $mod+d exec $menu

    bindsym $mod+p exec $drun



    bindsym $mod+r exec $bemenu
    bindsym $mod+w exec $browser

    bindsym $mod+e exec "emacs"

    bindsym $mod+v exec "setrandom $HOME/wallpapers"

    bindsym $mod+Shift+Return exec $fileman

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+r reload

    # Exit sway (logs you out of your Wayland session)
    #bindsym $mod+Shift+q exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'
    #bindsym $mod+Shift+q exec swaymsg exit
    bindsym $mod+Shift+q exec $logout

#
# Media/Special Keys:
#
    bindsym XF86AudioRaiseVolume exec amixer set Master 1%+
    bindsym XF86AudioLowerVolume exec amixer set Master 1%-
    bindsym XF86AudioMute exec amixer set Master toggle
    bindsym XF86MonBrightnessDown exec brightnessctl set 1%-
    bindsym XF86MonBrightnessUp exec brightnessctl set 1%+

#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus left
    bindsym $mod+$up focus right
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    #bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    #bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    #bindsym $mod+b splith
    #bindsym $mod+v splitv

    # Switch the current container between different layout styles
    #bindsym $mod+s layout stacking
    #bindsym $mod+w layout tabbed
    #bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+f floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
#bindsym $mod+r mode "resize"

client.focused "#000000" "#000000" "#000000" "#467b96" "#467b96"
client.focused_inactive "#000000" "#000000" "#000000" "#889fa7" "#889fa7"
client.unfocused "#000000" "#000000" "#000000" "#000000" "#889fa7"
client.focused_tab_title "#000000" "#000000" "#000000" "#000000" "#467b96"
client.urgent "#000000" "#000000" "#000000" "#000000" "#467b96"
#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
	#Either use waybar with the swaybar_command line, or use the rest of the config, which uses xmobar.
	# The original Doom Config
	#swaybar_command "waybar -c /home/shatterstone/.config/waybar/config-sway-doom -s /home/shatterstone/.config/waybar/style.css.doom &"
	# A DT-Inspired Config
	#swaybar_command waybar -c $HOME/.config/waybar/config-sway-dt -s $HOME/.config/waybar/style.css.dt &
	# My own Powerline Config
	#swaybar_command waybar -c $HOME/.config/waybar/config-sway-powerline -s $HOME/.config/waybar/style.css.arrow &
#	status_command xmobar -TSwaybar /home/shatterstone/.config/xmobar/xmobarrc-sway
#	font Ubuntu Bold 11
#	height 20
#	position top
#	icon_theme "Arc-Darkest-3.38"
#	colors { 
#		background "#282c34"
#		separator "#282c34"
#		focused_workspace "#282c34" "#1175b9" "#dfdfdf"
#		active_workspace "#282c34" "#282c34" "#dfdfdf"
#		inactive_workspace "#282c34" "#282c34" "#889fa7"
#		}
}
#exec waybar -c /home/shatterstone/.config/waybar/config-sway-powerline & 

exec waybar -c /home/shatterstone/.config/waybar/bubbles/config-sway-fedora -s /home/shatterstone/.config/waybar/bubbles/style-fedora.css &
exec nm-applet
exec_always autotiling -l 2
include @sysconfdir@/sway/config.d/*
