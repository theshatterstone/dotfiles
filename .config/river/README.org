#+TITLE: River Config
#+AUTHOR: Aleks (theshatterstone)
#+PROPERTY: header-args :tangle init
#+auto_tangle: t
#+STARTUP: showeverything
#+OPTIONS: toc:2


* River Configuration

The River configuration file is done using the riverctl utility so it can be written in any language, in this case, in shell script as it's the easiest.

#+begin_src sh
#!/bin/sh
#+end_src

* Input Rules

Note: the "Super" modifier is also known as Logo, GUI, Windows, Mod4, etc.

#+begin_src sh

riverctl keyboard-layout gb
riverctl xcursor-theme "Adwaita"
riverctl input pointer-1267-12610-ELAN0515:00_04F3:3142_Touchpad tap enabled
#+end_src

* Autostart

Sets up the wallpaper, bar and some other essentials.

#+begin_src sh

riverctl spawn "swaybg -m fill -i /home/shatterstone/wallpapers/21.jpg &"
#from wallutils package; requires swaybg (or feh for x11 sessions)
riverctl spawn "gammastep -O 5000"
riverctl spawn "nm-applet"
#riverctl spawn "setrandom $HOME/wallpapers"
# My Waybar Configs:
# The Original Doom Config
#riverctl spawn "waybar -c $HOME/.config/waybar/config-river-doom -s $HOME/.config/waybar/style.css.doom"
#eww open conky-doom
# A DT-Inspired Config
#riverctl spawn "waybar -c $HOME/.config/waybar/config-river-dt -s $HOME/.config/waybar/style.css.dt"
#eww open conky-doom
# My own Powerline Config
#riverctl spawn "waybar -c $HOME/.config/waybar/config-river-powerline -s $HOME/.config/waybar/style.css"
#eww open conky-powerline
# My Yambar Config
#riverctl spawn "yambar -c $HOME/.config/yambar/river.yml"
riverctl spawn "waybar -c $HOME/.config/waybar/bubbles/config-river-fedora -s $HOME/.config/waybar/bubbles/style-fedora.css"
#eww open conky-powerline
#+end_src

* Keybinds

#+begin_src sh

# File Manager
riverctl map normal Super+Shift Return spawn thunar
#riverctl map normal Super+Shift Return spawn "kitty -e ranger" 

# Music Player
riverctl map normal Super+Shift M spawn "foot -e cmus"
# Close the focused view
riverctl map normal Super+Shift C close

# Exit river
#riverctl map normal Super+Shift Q exit
riverctl map normal Super+Shift Q spawn "~/.local/bin/logout-script"

# Browser
#riverctl map normal Super W spawn "vivaldi --enable-features=UseOzonePlatform --ozone-platform=wayland"
#riverctl map normal Super W spawn "MOZ_ENABLE_WAYLAND=1 firefox" 
riverctl map normal Super W spawn flatpak run one.ablaze.floorp
#riverctl map normal Super W spawn floorp-ablaze

# Bemenu
#riverctl map normal Super R spawn "bemenu-run --hb '#467b96' --hf '#dfdfdf' --tb '#467b96' --tf '#dfdfdf' -H 20 --fn 'Hack' -p 'Run:'"
riverctl map normal Super R spawn "~/.local/bin/my-bemenu -r"

# Wofi Drun
#riverctl map normal Super P spawn "wofi --show drun"
riverctl map normal Super P spawn "~/.local/bin/my-bemenu -d"
#riverctl map normal Super P spawn "j4-dmenu-desktop --dmenu=\"bemenu --hb '#467b96' --hf '#dfdfdf' --tb '#467b96' --tf '#dfdfdf' -H 20 --fn 'Hack' -p 'Desktop:' -i -w --fork\" --term=kitty --usage-log=$HOME/.local/share/j4-dmenu-desktop.log --no-generic"

# Super+Return for Terminal
riverctl map normal Super Return spawn "foot"



riverctl map normal Super V spawn "setrandom $HOME/wallpapers"
riverctl map normal Super E spawn "emacs"

riverctl map normal Super Space swap # There is no swap-master option in river, use swap options defined below

# Super+J and Super+K to focus the next/previous view in the layout stack
riverctl map normal Super J focus-view next
riverctl map normal Super K focus-view previous

# Super+Shift+J and Super+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal Super+Shift J swap next
riverctl map normal Super+Shift K swap previous

# Super+Period and Super+Comma to focus the next/previous output
riverctl map normal Super Period focus-output next
riverctl map normal Super Comma focus-output previous

# Super+Shift+{Period,Comma} to send the focused view to the next/previous output
riverctl map normal Super+Shift Period send-to-output next
riverctl map normal Super+Shift Comma send-to-output previous

# Super+H and Super+L to decrease/increase the main ratio of rivertile(1)
riverctl map normal Super H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Super L send-layout-cmd rivertile "main-ratio +0.05"

# Super+Shift+H and Super+Shift+L to increment/decrement the main count of rivertile(1)
riverctl map normal Super+Shift H send-layout-cmd rivertile "main-count +1"
riverctl map normal Super+Shift L send-layout-cmd rivertile "main-count -1"

# Super+Alt+{H,J,K,L} to move views
riverctl map normal Super+Alt H move left 100
riverctl map normal Super+Alt J move down 100
riverctl map normal Super+Alt K move up 100
riverctl map normal Super+Alt L move right 100

# Super+Alt+Control+{H,J,K,L} to snap views to screen edges
riverctl map normal Super+Alt+Control H snap left
riverctl map normal Super+Alt+Control J snap down
riverctl map normal Super+Alt+Control K snap up
riverctl map normal Super+Alt+Control L snap right

# Super+Alt+Shift+{H,J,K,L} to resize views
riverctl map normal Super+Alt+Shift H resize horizontal -100
riverctl map normal Super+Alt+Shift J resize vertical 100
riverctl map normal Super+Alt+Shift K resize vertical -100
riverctl map normal Super+Alt+Shift L resize horizontal 100

# Super + Left Mouse Button to move views
riverctl map-pointer normal Super BTN_LEFT move-view

# Super + Right Mouse Button to resize views
riverctl map-pointer normal Super BTN_RIGHT resize-view

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))

    # Super+[1-9] to focus tag [0-8]
    riverctl map normal Super $i set-focused-tags $tags

    # Super+Shift+[1-9] to tag focused view with tag [0-8]
    riverctl map normal Super+Shift $i set-view-tags $tags

    # Super+Ctrl+[1-9] to toggle focus of tag [0-8]
    riverctl map normal Super+Control $i toggle-focused-tags $tags

    # Super+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal Super+Shift+Control $i toggle-view-tags $tags
done

# Super+0 to focus all tags
# Super+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 32) - 1))
riverctl map normal Super 0 set-focused-tags $all_tags
riverctl map normal Super+Shift 0 set-view-tags $all_tags

# Super+Space to toggle float
riverctl map normal Super+Shift F toggle-float

# Super+F to toggle fullscreen
riverctl map normal Super F toggle-fullscreen

# Super+{Up,Right,Down,Left} to change layout orientation
riverctl map normal Super Up    send-layout-cmd rivertile "main-location top"
riverctl map normal Super Right send-layout-cmd rivertile "main-location right"
riverctl map normal Super Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal Super Left  send-layout-cmd rivertile "main-location left"



# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

# Super+F11 to enter passthrough mode
riverctl map normal Super F11 enter-mode passthrough

# Super+F11 to return to normal mode
riverctl map passthrough Super F11 enter-mode normal

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # Eject the optical drive (well if you still have one that is)
    riverctl map $mode None XF86Eject spawn 'eject -T'

    # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'amixer set Master 1%+'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'amixer set Master 1%-'
    riverctl map $mode None XF86AudioMute         spawn 'amixer set Master toggle'

    # Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
    riverctl map $mode None XF86AudioMedia spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPlay  spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPrev  spawn 'playerctl previous'
    riverctl map $mode None XF86AudioNext  spawn 'playerctl next'

    # Control screen backlight brightness with light (https://github.com/haikarainen/light)
    riverctl map $mode None XF86MonBrightnessUp   spawn 'brightnessctl set 1.0%+'
    riverctl map $mode None XF86MonBrightnessDown spawn 'brightnessctl set 1.0%-'
done
#+end_src

* Borders

#+begin_src sh

# Set background and border color
#riverctl background-color 0x002b36
riverctl border-color-focused 0x474f54
riverctl border-color-unfocused 0x222222
riverctl border-width 2

# Set keyboard repeat rate
riverctl set-repeat 50 300
#+end_src

* Window Rules

#+begin_src sh

# Make certain views start floating
riverctl float-filter-add app-id float
riverctl float-filter-add title "popup title with spaces"
riverctl float-filter-add title "Bluetooth"
riverctl float-filter-add title "Bluetooth Manager"
riverctl float-filter-add title "Bluetooth Devices"
riverctl float-filter-add title "Open File"
# The following float rules (by app -id, taken from the AwesomeWM config) don't work
# riverctl float-filter-add app-id "Arandr"
# riverctl float-filter-add app-id "Blueberry"
# riverctl float-filter-add app-id "Galculator"
# riverctl float-filter-add app-id "Gnome-font-viewer"
# riverctl float-filter-add app-id "Gpick"
# riverctl float-filter-add app-id "Imagewriter"
# riverctl float-filter-add app-id "Font-manager"
# riverctl float-filter-add app-id "Kruler"
# riverctl float-filter-add app-id "MessageWin"
# riverctl float-filter-add app-id "Oblogout"
# riverctl float-filter-add app-id "Peek"
# riverctl float-filter-add app-id "Skype"
# riverctl float-filter-add app-id "System-config-printer.py"
# riverctl float-filter-add app-id "Sxiv"
# riverctl float-filter-add app-id "Unetbootin.elf"
# riverctl float-filter-add app-id "Wpa_gui"
# riverctl float-filter-add app-id "pinentry"
# riverctl float-filter-add app-id "veromix"
# riverctl float-filter-add app-id "xtightvncviewer"

# Set app-ids and titles of views which should use client side decorations
riverctl csd-filter-add app-id "gedit"
riverctl csd-filter-remove app-id "deadbeef"
# Enable sloppy focus
riverctl focus-follows-cursor always
#+end_src

* Layouts

#+begin_src sh

# Where new tiled windows appear in the stack: top for AwesomeWM Tile layout, bottom for MonadTall layout (XMonad and Qtile)
riverctl attach-mode bottom
# Set the default layout generator to be rivertile and start it.
# River will send the process group of the init executable SIGTERM on exit.
riverctl default-layout rivertile
#Define gaps
rivertile -view-padding 4 -outer-padding 4 -main-ratio 0.5 -main-count 1 -main-location "left"
#+end_src
