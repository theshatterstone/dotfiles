#
# spectrwm Example Configuration File
#
# PLEASE READ THE MAN PAGE BEFORE EDITING THIS FILE!
# https://htmlpreview.github.io/?https://github.com/conformal/spectrwm/blob/master/spectrwm.html
#
# All example settings in this file are commented out with a '#'.
# See the spectrwm(1) man page for default values.
#
# NOTE: rgb color values are in hexadecimal! See XQueryColor(3) for details.

workspace_limit	= 9
#focus_mode		= default
#focus_close		= previous
#focus_close_wrap	= 1
#focus_default		= last
#spawn_position		= next
#workspace_clamp	= 1
#warp_focus		= 1
#warp_pointer		= 1

# Include mapped workspaces when switching with any of the ws next/prev actions.
#cycle_visible		= 1

# Window Decoration
border_width		= 2
color_focus		= rgb:6f/6f/6f
#color_focus_maximized	= yellow
color_unfocus		= rgb:3f/3f/3f
#color_unfocus_maximized	= rgb:88/88/00
region_padding		= 8
tile_gap		= 8

# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
#boundary_width 		= 50

# Remove window border when bar is disabled and there is only one window in workspace
#disable_border		= 1

# Bar Settings
#bar_enabled		= 1
#bar_enabled_ws[1]	= 1
#bar_border_width	= 1
#bar_border[1]		= rgb:00/80/80
#bar_border_unfocus[1]	= rgb:00/40/40
bar_color[*]		= rgb:28/2c/34
#bar_color_selected[1]	= rgb:00/80/80
bar_font_color[1]	= rgb:df/df/df
#bar_font_color_selected	= black
bar_font		= Ubuntu:pixelsize=14:weight=bold:antialias=true
#bar_font_pua		= Typicons:pixelsize=14:antialias=true
#bar_action		= baraction.sh
#bar_action_expand	= 0
bar_justify		= center
bar_format		= +I +< +S +4< +W +5< %a %d %b %R
workspace_indicator	= listall,markcurrent,printnames
#workspace_mark_current	= '*'
#workspace_mark_active	= '^'
#workspace_mark_empty	= '-'
#workspace_mark_urgent	= '!'
#bar_at_bottom		= 1
stack_enabled		= 1
#stack_mark_horizontal	= '[-]'
#stack_mark_horizontal_flip	= '[v]'
#stack_mark_vertical	= '[|]'
#stack_mark_vertical_flip	= '[>]'
#stack_mark_max		= '[ ]'
#clock_enabled		= 1
#clock_format		= %a %b %d %R %Z %Y
#iconic_enabled		= 0
#maximize_hide_bar	= 0
#window_class_enabled	= 0
#window_instance_enabled	= 0
window_name_enabled	= 1
#verbose_layout		= 1
#urgent_enabled		= 1
#urgent_collapse	= 0

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
#dialog_ratio		= 0.6

# Split a non-RandR dual head setup into one region per monitor
# (non-standard driver-based multihead is not seen by spectrwm)
#region			= screen[1]:1280x1024+0+0
#region			= screen[1]:1280x1024+1280+0

# Launch applications in a workspace of choice
autorun			= ws[1]:feh --bg-fill --randomize /home/shatterstone/wallpapers
#autorun		= ws[1]:

# Customize workspace layout at start
#layout			= ws[1]:4:0:0:0:vertical
#layout			= ws[2]:0:0:0:0:horizontal
#layout			= ws[3]:0:0:0:0:fullscreen
#layout			= ws[4]:4:0:0:0:vertical_flip
#layout			= ws[5]:0:0:0:0:horizontal_flip

# Set workspace name at start
#name			= ws[1]:IRC
#name			= ws[2]:Email
#name			= ws[3]:Browse
#name			= ws[10]:Music

# Change the modifier to use when specifying 'MOD' in bindings.
# This should come before configuring bindings, not after.
# (Mod1: Alt key, Mod4: Windows key, Mod2: Apple key on OSX)
modkey = Mod4

# This allows you to include pre-defined key bindings for your keyboard layout.
# All key bindings are cleared before loading bindings in the specified file.
#keyboard_mapping	= ~/.spectrwm_us.conf

# PROGRAMS

# Validated default programs:
#program[lock]		= xlock
#program[term]		= xterm
#program[menu]		= dmenu_run $dmenu_bottom -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected
#program[search]	= dmenu $dmenu_bottom -i -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected
#program[name_workspace]	= dmenu $dmenu_bottom -p Workspace -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected

# To disable validation of the above, free the respective binding(s):
#bind[]			= MOD+Shift+Delete	# disable lock
#bind[]			= MOD+Shift+Return	# disable term
#bind[]			= MOD+p			# disable menu

# Optional default programs that will only be validated if you override:
#program[screenshot_all]	= screenshot.sh full	# optional
#program[screenshot_wind]	= screenshot.sh window	# optional
#program[initscr]	= initscreen.sh			# optional

# EXAMPLE: Define 'firefox' action and bind to key.
program[browser]	= firefox
program[fileman]	= thunar
program[term]		= kitty
program[run]		= rofi -show run -display-run ' Run: '
program[drun]		= rofi -show drun -display-drun ' Desktop: '
program[change]		= feh --bg-fill ---randomize /home/shatterstone/wallpapers

bind[browser]		= MOD+w
bind[fileman]		= MOD+Shift+Return
bind[term]		= MOD+Return
bind[run]		= MOD+r
bind[drun]		= MOD+p
bind[bar_toggle]	= MOD+b
bind[change]		= MOD+v

bind[swap_main]		= MOD+space
bind[cycle_layout]	= MOD+Tab
bind[restart]		= MOD+Shift+r
bind[quit]		= MOD+Shift+q
bind[wind_del]		= MOD+Shift+c
bind[wind_kill]		= MOD+Shift+x

# QUIRKS
# Default quirks, remove with: quirk[class:name] = NONE
#quirk[MPlayer:xv]			= FLOAT + FULLSCREEN + FOCUSPREV
#quirk[OpenOffice.org 2.4:VCLSalFrame]	= FLOAT
#quirk[OpenOffice.org 3.0:VCLSalFrame]	= FLOAT
#quirk[OpenOffice.org 3.1:VCLSalFrame]	= FLOAT
#quirk[Firefox-bin:firefox-bin]		= TRANSSZ
#quirk[Firefox:Dialog]			= FLOAT
#quirk[Gimp:gimp]			= FLOAT + ANYWHERE
#quirk[XTerm:xterm]			= XTERM_FONTADJ
#quirk[xine:Xine Window]			= FLOAT + ANYWHERE
#quirk[Xitk:Xitk Combo]			= FLOAT + ANYWHERE
#quirk[xine:xine Panel]			= FLOAT + ANYWHERE
#quirk[Xitk:Xine Window]			= FLOAT + ANYWHERE
#quirk[xine:xine Video Fullscreen Window]	= FULLSCREEN + FLOAT
#quirk[pcb:pcb]				= FLOAT
