Config { 

   -- appearance
   font =         "xft:ubuntu:size=10:bold:antialias=true"
   , bgColor = "#000000"   -- Background color (black in this case)
   , alpha = 0             -- 0 for full transparency, up to 255 for opaque
   , fgColor =      "#dfdfdf"
   , position =     Static { xpos = 0, ypos = 0, width = 700, height = 22}
   , border =       NoBorder

   -- layout
   , sepChar =  "%"   -- delineator between plugin names and straight text
   , alignSep = "}{"  -- separator between left-right alignment
   , template = " <fc=#ff6c6b>%bright%</fc> <fc=#666666>|</fc> <fc=#98be65>%cpu%</fc> <fc=#666666>|</fc> <fc=#EBCB8B>%battery%</fc> <fc=#666666>|</fc> <fc=#da8548>%thermal0%</fc> <fc=#666666>|</fc> <fc=#c678dd>%memory%</fc> <fc=#666666>|</fc> <fc=#46d9ff>%date%</fc> "

   -- general behavior

   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      True    -- show on all desktops
   , overrideRedirect = False    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     False   -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled)

   -- plugins
   --   Numbers can be automatically colored according to their value. xmobar
   --   decides color based on a three-tier/two-cutoff system, controlled by
   --   command options:
   --     --Low sets the low cutoff
   --     --High sets the high cutoff
   --
   --     --low sets the color below --Low cutoff
   --     --normal sets the color between --Low and --High cutoffs
   --     --High sets the color above --High cutoff
   --
   --   The --template option controls how the plugin is displayed. Text
   --   color can be set by enclosing in <fc></fc> tags. For more details
   --   see http://projects.haskell.org/xmobar/#system-monitor-plugins.

   , commands = [

        -- network activity monitor (dynamic interface resolution)
          Run DynNetwork     [ "--template" , "WiFi: <tx>kB/s|<rx>kB/s" ] 10

        -- cpu activity monitor
        , Run Cpu            [ "--template" , "Cpu: <total>%" ] 10

        , Run Brightness     [ "--template" , "Brightness: <percent>%"
                             , "--" --brightness specific options
                                       , "-D" , "amdgpu_bl1"
                             ] 10
        -- cpu core temperature monitor
        , Run ThermalZone 0  ["--template","Temp: <temp>C"] 30
                          
        -- memory usage monitor
        , Run Memory         [ "--template" , "Mem: <usedratio>%" ] 10

        -- , Run Volume         ["--template" , "Vol:<volume>%"] 10
        -- , Run Volume "default" "Master" 
        --                      [ "--template" , "Vol: <volume>%" ] 10
        -- battery monitor
        , Run Battery        [ "--template" , "Battery: <acstatus>"
                             -- , "--Low"      , "10"        -- units: %
                             -- , "--low"      , "red"
                             

                             , "--" -- battery specific options
                                       -- discharging status
                                       , "-o"	, "<left>%"
                                       -- AC "on" status
                                       , "-O"	, "<left>% Charging"
                                       -- charged status
                                       , "-i"	, "100%"
                             ] 50

        -- time and date indicator 
        --   (%F = y-m-d date, %a = day of week, %T = h:m:s time)
        , Run Date           " %a %d %b - %H:%M" "date" 10
        -- , Run Com ".config/xmobar/trayer-padding-icon.sh" [] "trayerpad" 20

        ]
}
