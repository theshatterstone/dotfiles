#!/bin/bash

cp $HOME/.config/qtile/config-theme2.py $HOME/.config/qtile/config.py

cp $HOME/.config/starship-theme2.toml $HOME/.config/starship.toml

cp $HOME/.config/fish/config-theme2.fish $HOME/.config/fish/config.fish
