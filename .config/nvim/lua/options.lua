-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Set to true if you have a Nerd Font installed and selected in the terminal
vim.g.have_nerd_font = true

-- [[ Setting options ]]
-- See `:help vim.opt`
-- NOTE: You can change these options as you wish!
--  For more options, you can see `:help option-list`

-- NOTE: This theme setting will only work if the doom-one theme plugin or any other theme plugin is not set/enabled
-- Remember to also set the lualine theme accordingly
-- vim.cmd.colorscheme 'shattermacs-dark'
require("themes.shattermacs-dark-green").setup()
local mytheme = require("themes.shattermacs-dark")

-- Make line numbers default
vim.opt.number = true
-- You can also add relative line numbers, to help with jumping.
--  Experiment for yourself to see if you like it!
vim.opt.relativenumber = true

-- Enable mouse mode, can be useful for resizing splits for example!
vim.opt.mouse = "a"

-- Highlight search terms as they're typed (Thanks Prime)
vim.opt.incsearch = true

-- Don't show the mode, since it's already in the status line
vim.opt.showmode = false

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.opt.clipboard = "unnamedplus"

-- Enable break indent
vim.opt.breakindent = true

-- Save undo history
vim.opt.undofile = true

-- Case-insensitive searching UNLESS \C or one or more capital letters in the search term
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Keep signcolumn on by default
vim.opt.signcolumn = "yes"

-- Set tabstop to 4
vim.opt.tabstop = 4

-- Decrease update time
vim.opt.updatetime = 250

-- Decrease mapped sequence wait time
-- Displays which-key popup sooner
vim.opt.timeoutlen = 300

-- Configure how new splits should be opened
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Enable termguicolors for nvim-colorizer
vim.opt.termguicolors = true

-- Sets how neovim will display certain whitespace characters in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
vim.opt.list = true
vim.opt.listchars = { tab = "» ", trail = "·", nbsp = "␣" }

-- Preview substitutions live, as you type!
vim.opt.inccommand = "split"

-- Show which line your cursor is on
vim.opt.cursorline = true

-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 10

-- Set Neovim Window Title
vim.opt.title = true
vim.opt.titlelen = 0 -- do not shorten title
vim.opt.titlestring = "%f - Shattervim"

-- Colors for org-mode's headlines plugin
-- Meant to be paired with my theme so they look good
-- vim.cmd(string.format("highlight Headline1 guibg=%s", mytheme.colors.bg_blue_primary))
-- vim.cmd [[highlight Headline2 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline3 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline4 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline5 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline6 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline7 guibg=#1c1f24]]
-- vim.cmd [[highlight Headline8 guibg=#1c1f24]]
