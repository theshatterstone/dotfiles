-- Shattermacs-Dark Neovim Theme (Fixed)
-- Generated based on the Emacs theme provided

local colors = {
	green_primary = "#92cf9c",
	bg_primary = "#282c34", -- Unchanged
	bg_secondary = "#1c1f24",
	bg_secondary_darker = "#21252b",
	fg_primary = "#abb2bf",
	fg_secondary = "#5c6370",
	green_secondary = "#4e9a06",
	green_tertiary = "#73b36b",
	green_quaternary = "#3a7403",
	bg_green_primary = "#2f3f30",
	bg_green_secondary = "#293a29",
	bg_green_tertiary = "#2c4b2f",
	bg_green_quaternary = "#223b26",
}

local theme = {}

theme.load_syntax = function()
	local syntax = {
		Normal = { fg = colors.fg_primary, bg = colors.bg_primary }, -- Explicit background color setting
		Comment = { fg = colors.fg_secondary, italic = true },
		Constant = { fg = colors.green_quaternary },
		String = { fg = colors.green_secondary },
		Identifier = { fg = colors.green_primary },
		Function = { fg = colors.green_tertiary },
		Statement = { fg = colors.green_primary, bold = true },
		Keyword = { fg = colors.green_primary, bold = true },
		Type = { fg = colors.green_secondary, bold = true },
		Special = { fg = colors.green_tertiary },
		Underlined = { fg = colors.green_tertiary, underline = true },
		Ignore = { fg = colors.bg_secondary },
		Error = { fg = colors.green_primary, bg = colors.bg_secondary_darker, bold = true },
		Todo = { fg = colors.green_primary, bg = colors.bg_secondary_darker, bold = true },
	}

	for group, conf in pairs(syntax) do
		vim.api.nvim_set_hl(0, group, conf)
	end
end

theme.load_treesitter = function()
	local ts = {
		TSComment = { fg = colors.fg_secondary, italic = true },
		TSConstant = { fg = colors.green_quaternary },
		TSString = { fg = colors.green_secondary },
		TSFunction = { fg = colors.green_tertiary },
		TSKeyword = { fg = colors.green_primary, bold = true },
		TSVariable = { fg = colors.green_primary },
		TSParameter = { fg = colors.green_primary },
		TSType = { fg = colors.green_secondary, bold = true },
		TSPunctBracket = { fg = colors.fg_primary },
		TSConditional = { fg = colors.green_primary, bold = true },
		TSRepeat = { fg = colors.green_primary, bold = true },
		TSOperator = { fg = colors.green_primary },
		TSException = { fg = colors.green_primary },
		TSField = { fg = colors.green_primary },
		TSProperty = { fg = colors.green_primary },
	}

	for group, conf in pairs(ts) do
		vim.api.nvim_set_hl(0, group, conf)
	end
end

theme.load_orgmode = function()
	-- Define own colors
	vim.api.nvim_set_hl(0, "@OrgTSHeadlineLevel1", { bg = colors.bg_green_primary, fg = colors.green_primary })
	vim.api.nvim_set_hl(0, "@OrgTSHeadlineLevel2", { bg = colors.bg_green_secondary, fg = colors.green_secondary })
	vim.api.nvim_set_hl(0, "@OrgTSHeadlineLevel3", { bg = colors.bg_green_tertiary, fg = colors.green_tertiary })
	vim.api.nvim_set_hl(0, "@OrgTSHeadlineLevel4", { bg = colors.bg_green_quaternary, fg = colors.green_quaternary })
	vim.api.nvim_set_hl(0, "@org.headline.level1", { bg = colors.bg_green_primary, fg = colors.green_primary })
	vim.api.nvim_set_hl(0, "@org.headline.level2", { bg = colors.bg_green_secondary, fg = colors.green_secondary })
	vim.api.nvim_set_hl(0, "@org.headline.level3", { bg = colors.bg_green_tertiary, fg = colors.green_tertiary })
	vim.api.nvim_set_hl(0, "@org.headline.level4", { bg = colors.bg_green_quaternary, fg = colors.green_quaternary })
	vim.api.nvim_set_hl(0, "@org.keyword.todo", { fg = colors.fg_primary })
	vim.api.nvim_set_hl(0, "@org.edit_src", { bg = colors.bg_secondary_darker })
	vim.api.nvim_set_hl(0, "@org.code", { bg = colors.bg_secondary_darker })
	-- Link to another highlight group
	-- vim.api.nvim_set_hl(0, '@org.agenda.scheduled_past', { link = 'Statement' })

	-- headlines.nvim colours config
	-- vim.cmd [[highlight Headline1 guibg=colors.bg_green_primary ]]
	-- vim.cmd [[highlight Headline2 guibg=colors.bg_green_secondary ]]
	-- vim.cmd [[highlight Headline3 guibg=colors.bg_green_tertiary ]]
	-- vim.cmd [[highlight Headline4 guibg=colors.bg_green_quaternary ]]
	-- vim.cmd [[highlight Headline5 guibg=colors.bg_green_primary ]]
	-- vim.cmd [[highlight Headline6 guibg=colors.bg_green_secondary ]]
	-- vim.cmd [[highlight Headline7 guibg=colors.bg_green_tertiary ]]
	-- vim.cmd [[highlight Headline8 guibg=colors.bg_green_quaternary ]]
	--

	-- vim.cmd [[highlight Headline1 guibg="#3c4a5a"]] -- Background for green_primary (#51afef)
	-- vim.cmd [[highlight Headline2 guibg="#344b5c" ]] -- Background for green_secondary (#569cd6)
	-- vim.cmd [[highlight Headline3 guibg="#375662" ]] -- Background for green_tertiary (#46d9ff)
	-- vim.cmd [[highlight Headline4 guibg="#2b4557" ]] -- Background for green_quaternary (#0f7ecc)

	vim.cmd(
		string.format("highlight RenderMarkdownH1 guibg=%s guifg=%s", colors.bg_green_primary, colors.green_primary)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH2 guibg=%s guifg=%s", colors.bg_green_secondary, colors.green_secondary)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH3 guibg=%s guifg=%s", colors.bg_green_tertiary, colors.green_tertiary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH4 guibg=%s guifg=%s",
			colors.bg_green_quaternary,
			colors.green_quaternary
		)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH5 guibg=%s guifg=%s", colors.bg_green_primary, colors.green_primary)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH6 guibg=%s guifg=%s", colors.bg_green_secondary, colors.green_secondary)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH7 guibg=%s guifg=%s", colors.bg_green_tertiary, colors.green_tertiary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH8 guibg=%s guifg=%s",
			colors.bg_green_quaternary,
			colors.green_quaternary
		)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH1BG guibg=%s guifg=%s", colors.bg_green_primary, colors.green_primary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH2BG guibg=%s guifg=%s",
			colors.bg_green_secondary,
			colors.green_secondary
		)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH3BG guibg=%s guifg=%s", colors.bg_green_tertiary, colors.green_tertiary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH4BG guibg=%s guifg=%s",
			colors.bg_green_quaternary,
			colors.green_quaternary
		)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH5BG guibg=%s guifg=%s", colors.bg_green_primary, colors.green_primary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH6BG guibg=%s guifg=%s",
			colors.bg_green_secondary,
			colors.green_secondary
		)
	)
	vim.cmd(
		string.format("highlight RenderMarkdownH7BG guibg=%s guifg=%s", colors.bg_green_tertiary, colors.green_tertiary)
	)
	vim.cmd(
		string.format(
			"highlight RenderMarkdownH8BG guibg=%s guifg=%s",
			colors.bg_green_quaternary,
			colors.green_quaternary
		)
	)

	vim.cmd(string.format("highlight Headline1 guibg=%s guifg=%s", colors.bg_green_primary, colors.green_primary))
	vim.cmd(string.format("highlight Headline2 guibg=%s guifg=%s", colors.bg_green_secondary, colors.green_secondary))
	vim.cmd(string.format("highlight Headline3 guibg=%s guifg=%s", colors.bg_green_tertiary, colors.green_tertiary))
	vim.cmd(string.format("highlight Headline4 guibg=%s guifg=%s", colors.bg_green_quaternary, colors.green_quaternary))

	vim.cmd(string.format("highlight RenderMarkdownCode guibg=%s", colors.bg_secondary_darker))
	vim.cmd(string.format("highlight RenderMarkdownCodeInline guibg=%s", colors.bg_secondary_darker))
	vim.cmd(string.format("highlight CodeBlock guibg=%s", colors.bg_secondary_darker))
	vim.cmd(string.format("highlight Dash guibg=%s gui=bold", colors.bg_green_primary))
	vim.cmd(string.format("highlight Quote guibg=%s gui=bold", colors.green_primary))

	vim.cmd(string.format("highlight DashboardHeader guifg=%s ", colors.green_primary))
	vim.cmd(string.format("highlight DashboardFooter guifg=%s ", colors.green_primary))
	vim.cmd(string.format("highlight DashboardProjectTitleIcon guifg=%s ", colors.green_primary))
	vim.cmd(string.format("highlight DashboardProjectTitle guifg=%s ", colors.green_primary))
	vim.cmd(string.format("highlight DashboardMruIcon guifg=%s ", colors.green_primary))
	vim.cmd(string.format("highlight DashboardMruTitle guifg=%s ", colors.green_primary))
end

theme.setup = function()
	theme.load_syntax()
	theme.load_treesitter()
	theme.load_orgmode()
end

return theme
