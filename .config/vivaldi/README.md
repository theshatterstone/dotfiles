# Guide to Setting up Vivaldi (for personal use)

1. Log into Vivaldi Account (should set up most settings automagically)

2. Ensure correct permissions if Flatpak

3. Import theme from dotfiles aka .config/vivaldi/

4. Enable CSS Mods via `vivaldi:experiments` and add .config/vivaldi to it in Settings (Search for CSS and it'll show up)

5. Add My Workspaces

6. Add Missing Keybinds, specifically:

    - Moving Up/Down through Tabs list (Ctrl-Up/Down)
    - Moving Tabs Up/Down in List (Ctrl-S-Up/Down)
    - Workspace Switching (Ctrl-Shift-num) (already enabled)
    - Duplicate Selected (current) Tab (Ctrl-D) (remember to disable the conflicting "Create bookmark" binding first)

