--[[
####################################
##  doom.conkyrc (XMonad)  ##
####################################
]]

conky.config = {

	--Various settings
	background = true, 							-- forked to background
	cpu_avg_samples = 2,						-- The number of samples to average for CPU monitoring.
	diskio_avg_samples = 10,					-- The number of samples to average for disk I/O monitoring.
	double_buffer = true,						-- Use the Xdbe extension? (eliminates flicker)
	if_up_strictness = 'address',				-- how strict if testing interface is up - up, link or address
	net_avg_samples = 2,						-- The number of samples to average for net data
	no_buffers = true,							-- Subtract (file system) buffers from used memory?
	temperature_unit = 'celsius',			-- fahrenheit or celsius
	text_buffer_size = 2048,					-- size of buffer for display of content of large variables - default 256
	update_interval = 1,						-- update interval
	imlib_cache_size = 0,                       -- disable image cache to get a new spotify cover per song

	-- Placement (Conky on MIDDLE of THREE monitors at 1920x1080)
	--alignment = 'top_left',		                -- top_left,top_middle,top_right,bottom_left,bottom_middle,bottom_right,
	--gap_x = 3540,                               -- pixels between right or left border
	--gap_y = 70,									-- pixels between bottom or left border

    -- Placement (For SINGLE monitor users!)
	alignment = 'top_right',		            -- top_left,top_middle,top_right,bottom_left,bottom_middle,bottom_right,
	gap_x = 19,								-- pixels between right or left border
	gap_y = 108,								-- pixels between bottom or left border

    -- Size
    minimum_height = 200,						-- minimum height of window
	minimum_width = 260,						-- minimum width of window
	maximum_width = 260,						-- maximum width of window

	--Graphical
	border_inner_margin = 5, 					-- margin between border and text
	border_outer_margin = 5, 					-- margin between border and edge of window
	border_width = 0, 							-- border width in pixels
	default_bar_width = 260,					-- default is 0 - full width
	default_bar_height = 10,					-- default is 6
	default_gauge_height = 25,					-- default is 25
	default_gauge_width =40,					-- default is 40
	default_graph_height = 40,					-- default is 25
	default_graph_width = 153,					-- default is 0 - full width
	default_shade_color = '#000000',			-- default shading colour
	default_outline_color = '#000000',			-- default outline colour
	draw_borders = false,						-- draw borders around text
	draw_graph_borders = true,					-- draw borders around graphs
	draw_shades = false,						-- draw shades
	draw_outline = false,						-- draw outline
	stippled_borders = 0,						-- dashing the border

	--Textual
	format_human_readable = true,				-- KiB, MiB rather then number of bytes
	font  = 'Ubuntu:bold:size=10.5',  	-- the default font used
	font2 = 'Ubuntu:bold:size=32',         	-- font for the time
	font3 = 'Ubuntu:bold:size=18',               	-- font for the date
	font4 = 'Ubuntu:bold:size=11.5',              -- font for the keybindings heading
	font5 = 'Ubuntu:bold:size=13',              -- font for the info
	max_text_width = 0,							-- 0 will make sure line does not get broken if width too smal
	max_user_text = 16384,						-- max text in conky default 16384
	override_utf8_locale = true,				-- force UTF8 requires xft
	short_units = true,							-- shorten units from KiB to k
	top_name_width = 21,						-- width for $top name value default 15
	top_name_verbose = false,					-- If true, top name shows the full command line of  each  process - Default value is false.
	uppercase = false,							-- uppercase or not
	use_spacer = 'none',						-- adds spaces around certain objects to align - default none
	use_xft = true,								-- xft font - anti-aliased font
	xftalpha = 1,								-- alpha of the xft font - between 0-1

	--Windows
	own_window = true,							-- create your own window to draw
	own_window_argb_value = 255,			    -- real transparency - composite manager required 0-255
	own_window_argb_visual = true,				-- use ARGB - composite manager required
	own_window_class = 'Conky',					-- manually set the WM_CLASS name for use with xprop
	own_window_colour = '#282c34',				-- set colour if own_window_transparent no
	own_window_transparent = false,				-- if own_window_argb_visual is true sets background opacity 0%
	own_window_title = 'xmonad_conky',			-- set the name manually
	own_window_type = 'override',				-- if own_window true options are: normal/override/dock/desktop/panel
	own_window_hints = 'undecorated,below,above,sticky,skip_taskbar,skip_pager',  -- if own_window true - just hints - own_window_type sets it

	--Colors (Doom One)
	color0 = '#c678dd',
	color1 = '#dfdfdf',
	color2 = '#51afef',
	color3 = '#98be65',
	color4 = '#46d9ff',
};

conky.text = [[
${alignc}${color0}${font2}${time %H:%M}${font}
${voffset 6}${alignc}${color0}${font3}${exec "date '+%d %B %Y'"}${font}${color}

${color3} ${font5} ${goto 30} Kernel: ${goto 150}${color4}${font5}$kernel${color}
${color3} ${font5} ${goto 30} Uptime: ${goto 150}${color4}${font5}$uptime${color}
${color3} ${font5} ${goto 30} Disk: ${goto 150}${color4}${font5}${exec "df -H | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $6 " : " $3 "/" $2 }' | grep "/ " | cut -d : -f 2 | cut -d " " -f 2"}${color}
${voffset 18}${color1}${alignc}${font4}KEYBINDINGS${font}${color}
${color1}${hr}${color}
${color1}${goto 20}[S] + RETURN${goto 140}${color2}Open Terminal${color}
${color1}${goto 20}[S] + W${goto 140}${color2}Open Browser${color}
${color1}${goto 20}[S] + SHIFT + RET${goto 140}${color2}Open File Manager${color}
${color1}${goto 20}[S] + R${goto 140}${color2}Open Run launcher${color}
${color1}${goto 20}[S] + P${goto 140}${color2}Open Drun launch${color}
${color1}${goto 20}[S] + SHIFT + C${goto 140}${color2}Close Win${color}
${color1}${hr}${color}
${color1}${goto 20}[S] + j/k${goto 140}${color2}Move Win Focus${color}
${color1}${goto 20}[S] + SHIFT + hjkl${goto 140}${color2}Move Window${color}
${color1}${goto 20}[S] + 1-9${goto 140}${color2}Switch Workspace${color}
${color1}${goto 20}[S] + SHIFT + 1-9${goto 140}${color2}Send Window${color}
${color1}${goto 20}[S] + h/l${goto 140}${color2}shrink/expand${color}
${color1}${goto 20}[S] + Space${goto 140}${color2}Swap to Master${color}
${color1}${hr}${color}
${color1}${goto 20}[S] + SHIFT+ R${goto 140}${color2}Restart WM${color}
${color1}${goto 20}[S] + SHIFT+ Q${goto 140}${color2}Quit WM${color}
]];
