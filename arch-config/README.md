# Arch Setup Guide

## Clone and set up repos

### Install git and fish:
```
sudo pacman -S git fish
```
### Install paru: 
```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```
Remember to 
``` 
cd ~ 
```
### Then clone the repos:
```
git clone https://gitlab.com/theshatterstone/dotfiles-fresh
git clone https://gitlab.com/theshatterstone/neovim
git clone https://gitlab.com/theshatterstone/linux-themes
git clone https://gitlab.com/theshatterstone/sddm
git clone https://gitlab.com/theshatterstone/dwm
```

### And finally set up the repos:
```
cp -r dotfiles-fresh/.config $HOME/
cp dotfiles-fresh/.bashrc $HOME/
cp -r neovim/.config $HOME/
cp -r neovim/local $HOME/
cp -r linux-themes/.themes $HOME/
cp -r linux-themes/.icons $HOME/
```
### Set up my zsh config (optional)
```
cp dotfiles-fresh/.zshrc ~/
cp -r dotfiles-fresh/.oh-my-zsh ~/
```
### In case it lists zsh-autosugggestions and zsh-syntax-highlighting as missing
```
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
```
### Set up my vim config (optional) !!! BROKEN !!!
```
cp neovim/.vimrc ~/
cp neovim/.viminfo ~/
cp -r neovim/.vim ~/
```

## Installing software
That's where the fun begins

WARNING: some of the following packages are from the AUR, so switch to my fish config and make sure to have paru installed before running the command below (also notice that there's no sudo, because the aforementioned method is the suggested way to run this command): 

```
pacman -S kitty awesome conky htop neofetch feh rofi dmenu thunar qtile brightnessctl python-pywayland python-pywlroots xorg-xwayland lxappearance nwg-look codium ttf-ubuntu-font-family ttf-hack xfce4-screenshooter blueman codium discord powertop gvfs wayshot grim grimshot wl-clipboard xclip sddm vlc android-file-transfer samba libwbclient bemenu wofi nitroshare meld qbittorrent cups xorg-xlsclients hyprland bc man-db xorg-xsetroot swaybg wallutils bspwm polybar sxhkd thunar-volman thunar-archive-plugin xmonad xmonad-contrib xmobar qt5-quickcontrols2 qt5-graphicaleffects brave-bin cmus mypy neovim
```
Note: ufetch-git is available on the AUR, but fails to build, so it is not in the command above. 
Install manually.

### Install ufetch manually:
1. Check the following link for any issues with the script before curl-ing it: https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-arch 

2. If no issues are found, proceed with installation: 

Global:
```
sudo curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-arch -o /usr/bin/ufetch && sudo chmod +x /usr/bin/ufetch
```
Local:
```
mkdir $HOME/.local/bin
sudo curl https://gitlab.com/jschx/ufetch/-/raw/main/ufetch-arch -o $HOME/.local/bin/ufetch && sudo chmod +x $HOME/.local/bin/ufetch
```

### Install Starship prompt
```
curl -sS https://starship.rs/install.sh | sh
```

### Install bsp-layout for BSPWM
```
curl https://raw.githubusercontent.com/phenax/bsp-layout/master/install.sh | bash
``` 
### Install vim-plug Plugin Manager (for vim and neovim):

```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```