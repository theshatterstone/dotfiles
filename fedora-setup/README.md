## Fedora Setup Guide

### Update system

```
sudo dnf update
```

### Change hostname

```
hostnamectl set-hostname <hostname goes here>
```

### Enable RPMFusion

Free:
```
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
```

NonFree:
```
sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

### Clone and set up repos

Install git:
```
sudo dnf install git
```

Then clone the repos:
```
git clone https://gitlab.com/theshatterstone/dotfiles
git clone https://gitlab.com/theshatterstone/neovim
git clone https://gitlab.com/theshatterstone/linux-themes
git clone https://gitlab.com/theshatterstone/sddm
git clone https://gitlab.com/theshatterstone/dwm
git clone https://gitlab.com/theshatterstone/doom-conf
```

And finally set up the repos:
```
cp -r dotfiles/.config $HOME/
cp dotfiles/.bashrc $HOME/
cp dotfiles/.zshrc $HOME/
cp -r neovim/.config $HOME/
cp -r neovim/local $HOME/
cp -r linux-themes/.themes $HOME/
cp -r linux-themes/.icons $HOME/

```

### Install basic software

The rest of the software:
```
 sudo dnf install kitty git pcmanfm thunar ranger cmus swaybg rofi wofi dmenu  bemenu awesome feh google-noto-sans-fonts waybar mypy ristretto conky htop neofetch brightnessctl lxappearance xfce4-screenshooter
```

VSCoduim/Code-OSS: Flatpak
```
flatpak install flathub com.vscodium.codium
```
### Copr repos:

```
dnf copr enable alebastr/river
dnf install river

dnf copr enable frostyx/qtile
dnf install qtile

dnf copr enable atim/ubuntu-fonts
dnf copr enable axeld/eww
dnf copr enable randalthor17/floorp
dnf copr enable solopasha/hyprland
dnf copr enable tofik/nwg-shell 

dnf install ubuntu-family-fonts eww-wayland-git floorp nwg-look 
```

### Install Hack and Ubuntu Nerd Fonts
Uses an install script from rajdeepsingh to install nerd-fonts
```
bash -c  "$(curl -fsSL https://raw.githubusercontent.com/officialrajdeepsingh/nerd-fonts-installer/main/install.sh)" 
```
### Install Starship prompt
```
curl -sS https://starship.rs/install.sh | sh
```

### Vim Plug:

```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

