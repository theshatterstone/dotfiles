﻿// Development
// #r "C:\Users\dalyisaac\Repos\workspacer\src\workspacer.Shared\bin\Debug\net5.0-windows\win10-x64\workspacer.Shared.dll"
// #r "C:\Users\dalyisaac\Repos\workspacer\src\workspacer.Bar\bin\Debug\net5.0-windows\win10-x64\workspacer.Bar.dll"
// #r "C:\Users\dalyisaac\Repos\workspacer\src\workspacer.Gap\bin\Debug\net5.0-windows\win10-x64\workspacer.Gap.dll"
// #r "C:\Users\dalyisaac\Repos\workspacer\src\workspacer.ActionMenu\bin\Debug\net5.0-windows\win10-x64\workspacer.ActionMenu.dll"
// #r "C:\Users\dalyisaac\Repos\workspacer\src\workspacer.FocusIndicator\bin\Debug\net5.0-windows\win10-x64\workspacer.FocusIndicator.dll"


// Production
//#r "C:\Program Files\workspacer\workspacer.Shared.dll"
//#r "C:\Program Files\workspacer\plugins\workspacer.Bar\workspacer.Bar.dll"
//#r "C:\Program Files\workspacer\plugins\workspacer.Gap\workspacer.Gap.dll"
//#r "C:\Program Files\workspacer\plugins\workspacer.ActionMenu\workspacer.ActionMenu.dll"
//#r "C:\Program Files\workspacer\plugins\workspacer.FocusIndicator\workspacer.FocusIndicator.dll"
//

#r "C:\Users\shatterstone\scoop\apps\workspacer\current\workspacer.Shared.dll"
#r "C:\Users\shatterstone\scoop\apps\workspacer\current\plugins\workspacer.Bar\workspacer.Bar.dll"
#r "C:\Users\shatterstone\scoop\apps\workspacer\current\plugins\workspacer.ActionMenu\workspacer.ActionMenu.dll"
#r "C:\Users\shatterstone\scoop\apps\workspacer\current\plugins\workspacer.FocusIndicator\workspacer.FocusIndicator.dll"
#r "C:\Users\shatterstone\scoop\apps\workspacer\current\plugins\workspacer.Gap\workspacer.Gap.dll"
#r "C:\Users\shatterstone\scoop\apps\workspacer\current\plugins\workspacer.TitleBar\workspacer.TitleBar.dll"

using System;
using System.Collections.Generic;
using System.Linq;
using workspacer;
using workspacer.Bar;
using workspacer.Bar.Widgets;
using workspacer.Gap;
using workspacer.ActionMenu;
using workspacer.FocusIndicator;
using workspacer.TitleBar;

return new Action<IConfigContext>((IConfigContext context) =>
{
    /* Variables */
    var fontSize = 10;
    var barHeight = 20;
    var fontName = "Ubuntu Bold";
    var background = new Color(0x28, 0x2c, 0x34);

    /* Config */
    context.CanMinimizeWindows = true;

    var titleBarPluginConfig = new TitleBarPluginConfig(new TitleBarStyle(showTitleBar: false, showSizingBorder: false));
    context.AddTitleBar(titleBarPluginConfig);

    /* Gap */
    var gap = barHeight - 8;
    var gapPlugin = context.AddGap(new GapPluginConfig() { InnerGap = gap, OuterGap = gap / 2, Delta = gap / 2 });

    /* Bar */
    context.AddBar(new BarPluginConfig()
    {
        FontSize = fontSize,
        BarHeight = barHeight,
        FontName = fontName,
        DefaultWidgetBackground = background,
        LeftWidgets = () => new IBarWidget[]
        {
            new WorkspaceWidget(), new TextWidget(": "), new TitleWidget() {
                IsShortTitle = true
            }
        },
        RightWidgets = () => new IBarWidget[]
        {
            new BatteryWidget(),
            new TimeWidget(1000, "HH:mm:ss dd-MMM-yyyy"),
            new ActiveLayoutWidget(),
        }
    });

    /* Bar focus indicator; briefly flashes a red border around the focused window 
     * when changing focus via Workspacer i.e focus keybinds or workspace switching */
    //context.AddFocusIndicator();

    /* Default layouts */
    Func<ILayoutEngine[]> defaultLayouts = () => new ILayoutEngine[]
    {
        new TallLayoutEngine(),
        //new VertLayoutEngine(),
        //new HorzLayoutEngine(),
        new FullLayoutEngine(),
    };

    context.DefaultLayouts = defaultLayouts;

    /* Workspaces */
    // Array of workspace names and their layouts
    (string, ILayoutEngine[])[] workspaces =
    {
        (" 1 ", defaultLayouts()),
        (" 2 ", defaultLayouts()),
        (" 3 ", defaultLayouts()),
        (" 4 ", defaultLayouts()),
        (" 5 ", defaultLayouts()),
        (" 6 ", defaultLayouts()),
        (" 7 ", defaultLayouts()),
        (" 8 ", defaultLayouts()),
        (" 9 ", defaultLayouts()),
    };

    foreach ((string name, ILayoutEngine[] layouts) in workspaces)
    {
        context.WorkspaceContainer.CreateWorkspace(name, layouts);
    }

    /* Filters */
    context.WindowRouter.AddFilter((window) => !window.ProcessFileName.Equals("1Password.exe"));
    context.WindowRouter.AddFilter((window) => !window.ProcessFileName.Equals("pinentry.exe"));

    // The following filter means that Edge will now open on the correct display
    context.WindowRouter.AddFilter((window) => !window.Class.Equals("ShellTrayWnd"));

    /* Routes */
    context.WindowRouter.RouteProcessName("Slack", "chat");
    context.WindowRouter.RouteProcessName("Discord", "chat");
    context.WindowRouter.RouteProcessName("Spotify", "🎶");
    context.WindowRouter.RouteProcessName("OUTLOOK", "cal");
    context.WindowRouter.RouteTitle("Microsoft To Do", "todo");

    /* Action menu */
    var actionMenu = context.AddActionMenu(new ActionMenuPluginConfig()
    {
        RegisterKeybind = false,
        MenuHeight = barHeight,
        FontSize = fontSize,
        FontName = fontName,
        Background = background,
    });

    /* Action menu builder */
    Func<ActionMenuItemBuilder> createActionMenuBuilder = () =>
    {
        var menuBuilder = actionMenu.Create();

        // Switch to workspace
//        menuBuilder.AddMenu("switch", () =>
//        {
//            var workspaceMenu = actionMenu.Create();
//            var monitor = context.MonitorContainer.FocusedMonitor;
//            var workspaces = context.WorkspaceContainer.GetWorkspaces(monitor);
//
//            Func<int, Action> createChildMenu = (workspaceIndex) => () =>
//            {
//                context.Workspaces.SwitchMonitorToWorkspace(monitor.Index, workspaceIndex);
//            };
//
//            int workspaceIndex = 0;
//            foreach (var workspace in workspaces)
//            {
//                workspaceMenu.Add(workspace.Name, createChildMenu(workspaceIndex));
//                workspaceIndex++;
//            }
//
//            return workspaceMenu;
//        });
//
//        // Move window to workspace
//        menuBuilder.AddMenu("move", () =>
//        {
//            var moveMenu = actionMenu.Create();
//            var focusedWorkspace = context.Workspaces.FocusedWorkspace;
//
//            var workspaces = context.WorkspaceContainer.GetWorkspaces(focusedWorkspace).ToArray();
//            Func<int, Action> createChildMenu = (index) => () => { context.Workspaces.MoveFocusedWindowToWorkspace(index); };
//
//            for (int i = 0; i < workspaces.Length; i++)
//            {
//                moveMenu.Add(workspaces[i].Name, createChildMenu(i));
//            }
//
//            return moveMenu;
//        });
//
//        // Rename workspace
//        menuBuilder.AddFreeForm("rename", (name) =>
//        {
//            context.Workspaces.FocusedWorkspace.Name = name;
//        });
//
//        // Create workspace
//        menuBuilder.AddFreeForm("create workspace", (name) =>
//        {
//            context.WorkspaceContainer.CreateWorkspace(name);
//        });
//
//        // Delete focused workspace
//        menuBuilder.Add("close", () =>
//        {
//            context.WorkspaceContainer.RemoveWorkspace(context.Workspaces.FocusedWorkspace);
//        });
//
        // Workspacer
        menuBuilder.Add("toggle keybind helper", () => context.Keybinds.ShowKeybindDialog());
        menuBuilder.Add("toggle enabled", () => context.Enabled = !context.Enabled);
        menuBuilder.Add("restart", () => context.Restart());
        menuBuilder.Add("quit", () => context.Quit());

        return menuBuilder;
    };
    var actionMenuBuilder = createActionMenuBuilder();

    /* Keybindings */
    Action setKeybindings = () =>
    {
	// I have remapped Win/Super to Alt/Meta using PowerToys
        KeyModifiers winShift = KeyModifiers.Alt | KeyModifiers.Shift;
        KeyModifiers winCtrl = KeyModifiers.Alt | KeyModifiers.Control;
        KeyModifiers win = KeyModifiers.Alt;

        IKeybindManager manager = context.Keybinds;

        var workspaces = context.Workspaces;

        manager.UnsubscribeAll();
        //manager.Subscribe(MouseEvent.LButtonDown, () => workspaces.SwitchFocusedMonitorToMouseLocation());

        // Left, Right keys
        manager.Subscribe(winCtrl, Keys.Left, () => workspaces.SwitchToPreviousWorkspace(), "switch to previous workspace");
        manager.Subscribe(winCtrl, Keys.Right, () => workspaces.SwitchToNextWorkspace(), "switch to next workspace");

        manager.Subscribe(winShift, Keys.Left, () => workspaces.MoveFocusedWindowToPreviousMonitor(), "move focused window to previous monitor");
        manager.Subscribe(winShift, Keys.Right, () => workspaces.MoveFocusedWindowToNextMonitor(), "move focused window to next monitor");

        // H, L keys
        manager.Subscribe(winShift, Keys.H, () => workspaces.FocusedWorkspace.ShrinkPrimaryArea(), "shrink primary area");
        manager.Subscribe(winShift, Keys.L, () => workspaces.FocusedWorkspace.ExpandPrimaryArea(), "expand primary area");

        manager.Subscribe(winCtrl, Keys.H, () => workspaces.FocusedWorkspace.DecrementNumberOfPrimaryWindows(), "decrement number of primary windows");
        manager.Subscribe(winCtrl, Keys.L, () => workspaces.FocusedWorkspace.IncrementNumberOfPrimaryWindows(), "increment number of primary windows");

        // K, J keys
        manager.Subscribe(winShift, Keys.K, () => workspaces.FocusedWorkspace.SwapFocusAndNextWindow(), "swap focus and next window");
        manager.Subscribe(winShift, Keys.J, () => workspaces.FocusedWorkspace.SwapFocusAndPreviousWindow(), "swap focus and previous window");

        manager.Subscribe(win, Keys.K, () => workspaces.FocusedWorkspace.FocusNextWindow(), "focus next window");
        manager.Subscribe(win, Keys.J, () => workspaces.FocusedWorkspace.FocusPreviousWindow(), "focus previous window");

        // Add, Subtract keys
        manager.Subscribe(winCtrl, Keys.Add, () => gapPlugin.IncrementInnerGap(), "increment inner gap");
        manager.Subscribe(winCtrl, Keys.Subtract, () => gapPlugin.DecrementInnerGap(), "decrement inner gap");

        manager.Subscribe(winShift, Keys.Add, () => gapPlugin.IncrementOuterGap(), "increment outer gap");
        manager.Subscribe(winShift, Keys.Subtract, () => gapPlugin.DecrementOuterGap(), "decrement outer gap");

	// Launcher Keys
	manager.Subscribe(win, Keys.Enter, () => System.Diagnostics.Process.Start("cmder"), "open terminal");
	manager.Subscribe(winShift, Keys.Enter, () => System.Diagnostics.Process.Start("explorer.exe"), "open file manager");
	manager.Subscribe(win, Keys.W, () => System.Diagnostics.Process.Start("firefox"), "open firefox");
        
	// Other shortcuts
        manager.Subscribe(winCtrl, Keys.P, () => actionMenu.ShowMenu(actionMenuBuilder), "show menu");
        manager.Subscribe(winShift, Keys.Escape, () => context.Enabled = !context.Enabled, "toggle enabled/disabled");
        manager.Subscribe(winShift, Keys.I, () => context.ToggleConsoleWindow(), "toggle console window");
	
        manager.Subscribe(win, Keys.T, () => context.Windows.ToggleFocusedWindowTiling(), "toggle tiling for focused window");
        manager.Subscribe(winShift, Keys.C, () => context.Workspaces.FocusedWorkspace.CloseFocusedWindow(), "close focused window");

	 
	// Workspace indexes start from 0
	// Keys to Switch to another Workspace
	manager.Subscribe(win, Keys.D1, () =>  context.Workspaces.SwitchToWorkspace(0), "switch to workspace 1");
	manager.Subscribe(win, Keys.D2, () =>  context.Workspaces.SwitchToWorkspace(1), "switch to workspace 2");
	manager.Subscribe(win, Keys.D3, () =>  context.Workspaces.SwitchToWorkspace(2), "switch to workspace 3");
	manager.Subscribe(win, Keys.D4, () =>  context.Workspaces.SwitchToWorkspace(3), "switch to workspace 4");
	manager.Subscribe(win, Keys.D5, () =>  context.Workspaces.SwitchToWorkspace(4), "switch to workspace 5");
	manager.Subscribe(win, Keys.D6, () =>  context.Workspaces.SwitchToWorkspace(5), "switch to workspace 6");
	manager.Subscribe(win, Keys.D7, () =>  context.Workspaces.SwitchToWorkspace(6), "switch to workspace 7");
	manager.Subscribe(win, Keys.D8, () =>  context.Workspaces.SwitchToWorkspace(7), "switch to workspace 8");
	manager.Subscribe(win, Keys.D9, () =>  context.Workspaces.SwitchToWorkspace(8), "switch to workspace 9");

	// Keys to move Focused Window to Workspace
	manager.Subscribe(winShift, Keys.D1, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(0), "move window to workspace 1");
	manager.Subscribe(winShift, Keys.D2, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(1), "move window to workspace 2");
	manager.Subscribe(winShift, Keys.D3, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(2), "move window to workspace 3");
	manager.Subscribe(winShift, Keys.D4, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(3), "move window to workspace 4");
	manager.Subscribe(winShift, Keys.D5, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(4), "move window to workspace 5");
	manager.Subscribe(winShift, Keys.D6, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(5), "move window to workspace 6");
	manager.Subscribe(winShift, Keys.D7, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(6), "move window to workspace 7");
	manager.Subscribe(winShift, Keys.D8, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(7), "move window to workspace 8");
	manager.Subscribe(winShift, Keys.D9, () =>  context.Workspaces.MoveFocusedWindowToWorkspace(8), "move window to workspace 9");
	
	// Restart and Quit
	manager.Subscribe(winShift, Keys.R, () =>  context.Restart(), "restart");
	manager.Subscribe(winShift, Keys.Q, () =>  context.Quit(), "quit");
    };
    setKeybindings();
});
